(function () {
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);

    RegCtrl.$inject = ["$http"];
    
    function RegCtrl($http) {
        var regCtrlself = this;
        
        regCtrlself.onSubmit = onSubmit;
        regCtrlself.initForm = initForm;
        regCtrlself.onlyFemale = onlyFemale;
        regCtrlself.user ={

        }
        regCtrlself.nationalities =[
            {name: "Please select", value: "0"},
            {name: "Singaporean", value: "1"},
            {name: "Malaysian", value: "2"},
            {name: "Indonesian", value: "3"},
            {name: "Thai", value: "4"},
            {name: "Others", value: "5"}
        ];
    
    function initForm() {
        regCtrlself.user.selectNationality="0";
        // this regCtrlself.... is to tell the function when they init form to auto show value 0
        regCtrlself.user.gender = "F";
        // this is to let the form be loaded with Female being checked and the span will not be activated. what is written in html is only at user interface.
        }
    
    function onlyFemale(){
            console.log("only female");
            return regCtrlself.user.gender=="F";
        }
    
    function onSubmit() {
        console.log(regCtrlself.user.email);
        console.log(regCtrlself.user.password);
        console.log(regCtrlself.user.confirmpassword);
        console.log(regCtrlself.user.fullName);
        console.log(regCtrlself.user.gender);
        console.log(regCtrlself.user.dob);
        console.log(regCtrlself.user.selectNationality);
        console.log(regCtrlself.user.contactNumber);
        $http.post("/api/submitForm",regCtrlself.user).then((result)=>{
                console.log("result>" + result);
                var registeredUser = result.data;
                console.log("email>" + registeredUser.email);
                console.log("Password>" + registeredUser.password);
                console.log("Confirm Password>" + registeredUser.confirmpassword);
                console.log("Full Name>" + registeredUser.fullName);
                console.log("Gender>" + registeredUser.gender);
                console.log("Date of birth>" + registeredUser.dob);
                console.log("Nationality>" + registeredUser.selectNationality);
                console.log("Contact Number>" + registeredUser.contactNumber);
                        
            }).catch((error)=>{
                console.log("error> " + error);             
            // ng-module for all items in index.html, post all items in the server side
            })
        
        
        
        }
    regCtrlself.initForm();
    }

})();