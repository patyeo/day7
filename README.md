Steps to commit codes
git add .
git commit -m “message”
git push origin master -u (origin is to bit bucket)

Other info about Git:
To clone git: @ terminal : git clone https://myuser.../sourcename.git )
To update my local copy: @ terminal : git pull
To overwrite my local copy (in conflicts and want to use the other person’s copy): 
@ terminal : git fetch –all
@ terminal : git reset – hard origin/master
@ terminal : git pull origin master

Cloud Deployment for new project
Login to heroku website
@ terminal c:\directory\file>heroku create
@ terminal : git remote add heroku <git add generated @ above step>
@ terminal : git remote -v (to check that I have 4 files)
@ terminal : git add .
@ terminal : commit -m “deploy to heroku”
@ terminal : git push heroku master -u
A heroku url should be generated. Use it to open a new web page to check that it is working.
Repeat bold steps when there’s changes done in vsc for it to be reflected at the web page.

New Project set up
Go to bit bucket to create new repo (get the git url)

@ terminal : ctrl c to go back to directory mode from nodemon
@ terminal : mkdir server directory
@ terminal : cd new directory
@ terminal : git init
@ terminal : git remote add origin <git url> 
@ terminal : npm init (answer all the questions, enter the entry point – server/xxx.js accordingly)
@ terminal : npm install –save express (to save the express library, node_modules auto generated into vsc local directory)
@ terminal : npm install body-parser –save (body parser is a decoder for the server and client to communicate)
@ terminal : npm I bower -g (to install bower globally)
@ terminal : bower init
@ terminal : bower install bootstrap font-awesome angular –save (tell bower to install package)
@ terminal : set NODE_PORT=3000 (or 4000)
@ terminal : nodemon (to see the code executed, check error)

@ vsc : create .igitignore to ignore directories / files (node_modules and client/bower components – don’t want to check into bit bucket, cannot be on server or client directory)
@ vsc : create client (folder), in client folder, create index.html, app folder to contain app.module.js and app.controller.js, css folder to contain main.css
@ vsc : create server (folder), in server folder, create app.js and quizes.json (data structure)
@ vsc : create README.md (file) – main directory to write notes
@ vsc : create .bowerrc - main directory 
In .bowerrc, keyed in {“directory”: “client/bower_components”} (to create own library so as to minimize problem when there is  difference in dependency’s version in client and server side library)
@ vsc : bower.json would show up automatically after bower is install
* ensure that bower_components is under client and .bowerrc and bower.json are in main directory.

in vsc: 
client / app / app.controller.js; client / app / app.module.js
client / bower_components
client / css / main.css
client / index.html
node_modules
server / app.js
.bowerrc
.gitignore (key in node_modules and client/bower_components)
.bower.json
.package.json
.README.md

javascript data structure need to be human readable, that I can then construct a webpage base on the javascript.
the codes written in javascript can be easily interpreted, understood by another coder. the variable name given need to give meaning to its reader


in index.html, add on
<!DOCTYPE html>
<html lang="en" ng-app="RegApp">tag this to turn this page into an angular app
    
    <head>
        <meta charset="UTF-8">
        <title>Registration</title>
        <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap-theme.css" />
        <link rel="stylesheet" href="/css/main.css" />
    </head>
    
    <body> controller can be place in body as well
    <div class="col-sm-2 col-lg-4"></div>
        <div ng-controller="RegCtrl as ctrl" class="col-sm-8 col-lg-4 registration-page"> so that the controller can be activated
            <h1> Registration </h1>
            <form name="registrationform" novalidate> </form>

and before end of body
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/angular/angular.min.js"></script>
<script src="js/app.js"></script> - use the name of the js in the client folder

go regexr.com to find and create regular expression to be used.

In app.module.js, must start with 
(function () {
    "use strict";
    angular.module("RegApp", []);  
})();
** RegApp is the name of the App created for the project to match with app.controller
name of directives to be added in the []. to add more, just add comma within the []

In app.controller.js, must start with
(function () {
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);

    RegCtrl.$inject = ["$http"];
    
    function RegCtrl($http) {
** functions to be within this function: specific functions in the controller.js file needed by the app**
    };
})();

Create end point (the back end) for the data to be posted to at server js file which looks like this
app.post("/api/submitQuizes",(req,res)=>{
    console.log("submit popquizes");
    console.log(req.body);
    res.status(200).json(popQuiz);
});

ensure that the end point $http.post("/api/submitQuizes",self.finalAnswer).then((result)=>{
                    self.isCorrect = result.data;
                }).catch((e)=>{
                    console.log(e);
in /client/controller.js match server/xxx.js so that they can connect.

Create a xxx.json file under server to put questions for the quiz
in quizes.json (json file cannot comment)
// most friendly data format for javascript
// need to double quote the var (key)
// answers are in array form and need to iterate to a format that can be read
